<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use View;
use DataTables;
use JWTAuth;

class UserController extends Controller
{

    protected $user;

    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }

    public function getUsers(Request $request){

        $user = User::paginate(10);

        if($user){
            return response()->json([
                'user' => $user
            ], 200);
        }else{
            return response()->json([
                'user' => "error"
            ], 400);
        }
    }

    public function show_user($id){

        $user = User::find($id);

        if($user){
            return response()->json([
                'user' => $user
            ], 200);
        }else{
            return response()->json([
                'user' => "error"
            ], 400);
        }
    }

    public function editUser(Request $request){

        $user = User::find($request->id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->fecha_nacimiento = $request->fecha_nacimiento;
        $user->phone = $request->phone;
        $user->username = $request->username;

        $user->save();

        if($user){
            return response()->json([
                'message' => "User updated"
            ], 200);
        }else{
            return response()->json([
                'message' => "User could not be updated"
            ], 400);
        }
    }

    public function removeUser($id){

        $user = User::find($id);
        $user->delete();

        if($user){
            return response()->json([
                'message' => "User deleted"
            ], 200);
        }else{
            return response()->json([
                'message' => "User could not be deleted"
            ], 400);
        }

    }
}
