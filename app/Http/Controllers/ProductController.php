<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use App\Product;
use View;
use DataTables;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\RegisterAuthRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class ProductController extends Controller
{
    
    public function createProduct(Request $request){

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'quantity' => ['required', 'numeric'],
            'price' => ['required', 'numeric'],
            
        ]);
 
        if ($validator->fails()) {
            return response()->json(['errors_validation'=> $validator->errors()]);
        }

        $prod = new Product();
        $prod->sku = $request->sku;
        $prod->name = $request->name;
        $prod->quantity = $request->quantity;
        $prod->price = $request->price;
        $prod->description = $request->description;
        $prod->image = $request->image;
        $prod->save();

        if($prod){
            return response()->json([
                'data' => $prod,
                'message' => "Product Created"
            ], 200);
        }else{
            return response()->json([
                'message' => "Product could not be created"
            ], 400);
        }
    }

    public function getProducts(){

        $product = Product::paginate(10);

        if($product){
            return response()->json([
                'prod' => $product
            ], 200);
        }else{
            return response()->json([
                'prod' => "error"
            ], 400);
        }
    }

    public function show_product($id){

        $prod = Product::find($id);

        if($prod){
            return response()->json([
                'prod' => $prod
            ], 200);
        }else{
            return response()->json([
                'prod' => "error"
            ], 400);
        }
    }

    public function editProduct(Request $request){

        $prod = Product::find($request->id);
        $prod->sku = $request->sku;
        $prod->name = $request->name;
        $prod->quantity = $request->quantity;
        $prod->price = $request->price;
        $prod->description = $request->description;
        $prod->image = $request->image;
        $prod->save();

        if($prod){
            return response()->json([
                'data' => $prod,
                'message' => "Product updated"
            ], 200);
        }else{
            return response()->json([
                'message' => "Product could not be updated"
            ], 400);
        }
    }

    public function remove($id){

        $prod = Product::find($id);
        $prod->delete();

        if($prod){
            return response()->json([
                'message' => "Product deleted"
            ], 200);
        }else{
            return response()->json([
                'message' => "Product could not be deleted"
            ], 400);
        }
    }

}
