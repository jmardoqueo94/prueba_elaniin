-- MySQL dump 10.13  Distrib 5.7.30, for Linux (x86_64)
--
-- Host: localhost    Database: prueba_elaniin
-- ------------------------------------------------------
-- Server version	5.7.30-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1),(4,'2020_05_14_193436_create_products_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
INSERT INTO `password_resets` VALUES ('jmardoqueo94@gmail.com','$2y$10$hr/3Z7e8Daf.v0iXmd.ol.DO5m1PTzQGO.4ZmbP3lsWdlu5wD8nwy','2020-05-17 00:32:56');
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `sku` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `quantity` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'61449','ut',43.43,7,'Product Description','https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTxybTeJCkLvNnZEZgG0IKtkj6GVj5gfu_A4i99IOvF4BDDI4fA&usqp=CAU','2020-05-15 23:05:45','2020-05-15 23:05:45'),(2,'31052','quibusdam',27.85,3,'Product Description','https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTxybTeJCkLvNnZEZgG0IKtkj6GVj5gfu_A4i99IOvF4BDDI4fA&usqp=CAU','2020-05-15 23:05:45','2020-05-15 23:05:45'),(3,'66327','omnis',16.08,2,'Product Description','https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTxybTeJCkLvNnZEZgG0IKtkj6GVj5gfu_A4i99IOvF4BDDI4fA&usqp=CAU','2020-05-15 23:05:45','2020-05-15 23:05:45'),(4,'307','exercitationem',37.31,2,'Product Description','https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTxybTeJCkLvNnZEZgG0IKtkj6GVj5gfu_A4i99IOvF4BDDI4fA&usqp=CAU','2020-05-15 23:05:45','2020-05-15 23:05:45'),(5,'45847','quibusdam',2.47,5,'Product Description','https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTxybTeJCkLvNnZEZgG0IKtkj6GVj5gfu_A4i99IOvF4BDDI4fA&usqp=CAU','2020-05-15 23:05:45','2020-05-15 23:05:45'),(6,'91833','omnis',39.51,3,'Product Description','https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTxybTeJCkLvNnZEZgG0IKtkj6GVj5gfu_A4i99IOvF4BDDI4fA&usqp=CAU','2020-05-15 23:05:45','2020-05-15 23:05:45'),(7,'60274','nihil',21.27,1,'Product Description','https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTxybTeJCkLvNnZEZgG0IKtkj6GVj5gfu_A4i99IOvF4BDDI4fA&usqp=CAU','2020-05-15 23:05:45','2020-05-15 23:05:45'),(8,'75180','odio',6.19,3,'Product Description','https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTxybTeJCkLvNnZEZgG0IKtkj6GVj5gfu_A4i99IOvF4BDDI4fA&usqp=CAU','2020-05-15 23:05:45','2020-05-15 23:05:45'),(9,'14010','voluptatem',22.44,9,'Product Description','https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTxybTeJCkLvNnZEZgG0IKtkj6GVj5gfu_A4i99IOvF4BDDI4fA&usqp=CAU','2020-05-15 23:05:45','2020-05-15 23:05:45'),(10,'94172','cumque',11.13,3,'Product Description','https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTxybTeJCkLvNnZEZgG0IKtkj6GVj5gfu_A4i99IOvF4BDDI4fA&usqp=CAU','2020-05-15 23:05:45','2020-05-15 23:05:45'),(11,'80491','deserunt',36.76,7,'Product Description','https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTxybTeJCkLvNnZEZgG0IKtkj6GVj5gfu_A4i99IOvF4BDDI4fA&usqp=CAU','2020-05-15 23:05:45','2020-05-15 23:05:45'),(12,'91232','temporibus',9.54,5,'Product Description','https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTxybTeJCkLvNnZEZgG0IKtkj6GVj5gfu_A4i99IOvF4BDDI4fA&usqp=CAU','2020-05-15 23:05:45','2020-05-15 23:05:45'),(13,'14640','corporis',19.43,6,'Product Description','https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTxybTeJCkLvNnZEZgG0IKtkj6GVj5gfu_A4i99IOvF4BDDI4fA&usqp=CAU','2020-05-15 23:05:45','2020-05-15 23:05:45'),(14,'14354','inventore',20.38,5,'Product Description','https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTxybTeJCkLvNnZEZgG0IKtkj6GVj5gfu_A4i99IOvF4BDDI4fA&usqp=CAU','2020-05-15 23:05:45','2020-05-15 23:05:45'),(15,'22867','voluptatem',8.07,8,'Product Description','https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTxybTeJCkLvNnZEZgG0IKtkj6GVj5gfu_A4i99IOvF4BDDI4fA&usqp=CAU','2020-05-15 23:05:45','2020-05-15 23:05:45'),(16,'1467','velit',41.11,3,'Product Description','https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTxybTeJCkLvNnZEZgG0IKtkj6GVj5gfu_A4i99IOvF4BDDI4fA&usqp=CAU','2020-05-15 23:05:45','2020-05-15 23:05:45'),(17,'54968','quod',39.95,8,'Product Description','https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTxybTeJCkLvNnZEZgG0IKtkj6GVj5gfu_A4i99IOvF4BDDI4fA&usqp=CAU','2020-05-15 23:05:45','2020-05-15 23:05:45'),(18,'27809','voluptatem',9.6,5,'Product Description','https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTxybTeJCkLvNnZEZgG0IKtkj6GVj5gfu_A4i99IOvF4BDDI4fA&usqp=CAU','2020-05-15 23:05:45','2020-05-15 23:05:45'),(19,'75587','at',37.81,6,'Product Description','https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTxybTeJCkLvNnZEZgG0IKtkj6GVj5gfu_A4i99IOvF4BDDI4fA&usqp=CAU','2020-05-15 23:05:45','2020-05-15 23:05:45'),(21,'15566','ps4',399.5,1,'PlayStation','https://psmedia.playstation.com/is/image/psmedia/ps4-overview-console-02-en-04sep18_1536139335903?$Icon$','2020-05-16 07:07:08','2020-05-16 07:07:08'),(22,'17566','ps6',600,1,'PlayStation 6','https://psmedia.playstation.com/is/image/psmedia/ps4-overview-console-02-en-04sep18_1536139335903?$Icon$','2020-05-16 07:07:37','2020-05-16 07:18:46');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_nacimiento` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Harley Dooley','1-702-942-9728 x8183','Torrance3','1996-01-14','iwalter@hotmail.com',NULL,'$2y$10$HkZfqL3wF8Z9lDa7PQ5VYed33dDzF5W6DK11zCdcXby25HBQNHgVW',NULL,'2020-05-15 23:05:29','2020-05-15 23:05:29'),(2,'Tyree Funk','306.800.2226 x661','Felix3','1996-01-19','jacobson.perry@hotmail.com',NULL,'$2y$10$ji1NadBhEZfG3HX6TQDtSOAixfl76uu9UOwBUqnoTDNQgVfK2TDbm',NULL,'2020-05-15 23:05:29','2020-05-15 23:05:29'),(3,'Rashawn Hoeger','1-764-667-5771 x456','Mavis6','1971-07-28','peyton10@bashirian.com',NULL,'$2y$10$sLyzEksp4zDiNkfn7iwfbehRM4Vtu1DmJ4I5CcChoDAJSV8aa9vWu',NULL,'2020-05-15 23:05:29','2020-05-15 23:05:29'),(4,'Colin Keebler','813-638-7012 x142','Josue2','2000-07-06','rippin.chase@kemmer.com',NULL,'$2y$10$eKvEswe94xgB6hx.eDu2UOCa07rSvqjwoQyrPvutVsIMoxd7qSdTK',NULL,'2020-05-15 23:05:29','2020-05-15 23:05:29'),(5,'Rico McKenzie','445-972-0078 x670','Hobart9','1975-10-15','katrina.steuber@yahoo.com',NULL,'$2y$10$AK2vTpAUsjYqrybaV0UViOu1tr1NFOM6f/tCkrT5Q2kVlHsALpOXu',NULL,'2020-05-15 23:05:29','2020-05-15 23:05:29'),(6,'Arden Heathcote','293-827-9504','Eduardo7','1970-12-05','meghan.lynch@bailey.com',NULL,'$2y$10$Hd7oZTpEXz5cQML.l1PrMOwDIyRZEr.yv1kGSc8hYt9rj5XH93vYi',NULL,'2020-05-15 23:05:29','2020-05-15 23:05:29'),(7,'Kevin Hoppe','1-452-389-5615','Cristina3','1978-02-03','hegmann.pasquale@olson.com',NULL,'$2y$10$WZ2Gam.7DBrf1k4SJS3n5u1VRBSZrMjRN5tiaJIiVVKr.gWexeqJS',NULL,'2020-05-15 23:05:29','2020-05-15 23:05:29'),(8,'Bertha Mitchell','287-490-4786','Darwin5','1993-12-27','stanton.chaz@donnelly.info',NULL,'$2y$10$2YJB3VY8OqEukjo.iJaCvOtUzF/E7V1mjZLbgeMEDJyjHb1vD5C3q',NULL,'2020-05-15 23:05:29','2020-05-15 23:05:29'),(9,'Johathan Bins','1-449-970-1350','Jake8','1974-05-27','zoey.reichert@bernier.com',NULL,'$2y$10$enoC.QdS9bQhC2K/Xab94.sLumL2d0stYCv2i917MC.Xoed3A7rjW',NULL,'2020-05-15 23:05:29','2020-05-15 23:05:29'),(10,'Tobin Sauer','+17538183722','Arthur0','1984-06-17','barney06@koss.com',NULL,'$2y$10$/9kO3ElkvfKZkB1Z3QOM3urUMwiInpp/.mRXI5E.p5T5UGb2aAs7.',NULL,'2020-05-15 23:05:29','2020-05-15 23:05:29'),(11,'Aidan Littel','+12453806938','Alan1','1987-01-04','samanta90@yahoo.com',NULL,'$2y$10$Ud23.ml2QabpTZT8ofqmQuWru2V5NtPMae.K/w3mllqZk8UPzQ706',NULL,'2020-05-15 23:05:29','2020-05-15 23:05:29'),(12,'Zachery Jacobs','1-573-330-8597 x398','Price3','1996-04-24','hmohr@koss.com',NULL,'$2y$10$StnQ05BB9ybLgjyHmKZzL.GgNvMt2fq/XvPLNuVp5A7x3wXOgFe9W',NULL,'2020-05-15 23:05:29','2020-05-15 23:05:29'),(13,'Cornelius Keeling','(460) 371-6019 x55663','Seth4','1994-07-12','elvie.gleichner@yahoo.com',NULL,'$2y$10$qjHUgYehdDGK0ZNow55IYeGGmyk.1ZU3b4I9pDvDjRtaSunl/g8ta',NULL,'2020-05-15 23:05:29','2020-05-15 23:05:29'),(14,'Matt Mann','1-491-561-7889 x179','Oral6','1979-08-31','xstamm@hotmail.com',NULL,'$2y$10$8j0i3yOnm8yQvt/FqbwnCeWdm0qAb5wJtYeFKuxBvfL0cNG0pIA9G',NULL,'2020-05-15 23:05:30','2020-05-15 23:05:30'),(15,'Garret Parker','(987) 702-6035 x405','Curtis3','1992-11-20','mossie.schiller@gottlieb.org',NULL,'$2y$10$wKfWisB7Q9ViE9TM8QbzKOIMwvwlBKUsXjFVHAj9Hr9T9ob7vmRSy',NULL,'2020-05-15 23:05:30','2020-05-15 23:05:30'),(16,'Mariano Stroman','1-354-871-8235 x059','Raoul6','1974-09-29','muhammad.reilly@casper.net',NULL,'$2y$10$Vw0AuV9gAAKNGORJMwMOYOeoqcVMPi0jHqhI.Iz4/4aVxnp.fgJ92',NULL,'2020-05-15 23:05:30','2020-05-15 23:05:30'),(17,'Quincy Towne','453.819.5029 x070','Lew6','1976-07-03','gcummerata@powlowski.biz',NULL,'$2y$10$WrP9xd1zjNeO5aKmExmhbuwxEKzKl1aMfKo.LH4iVe5x716EfjsXG',NULL,'2020-05-15 23:05:30','2020-05-15 23:05:30'),(18,'Cristopher Strosin','1-504-760-3367 x9923','Theodore8','1988-01-13','yrowe@hegmann.com',NULL,'$2y$10$e4snt0BxTQyqFoeZmLldG.bV/ltNmbGtqN49T1kKyjGRsWExxv.du',NULL,'2020-05-15 23:05:30','2020-05-15 23:05:30'),(19,'Marlin Rippin','1-371-590-4952','Arch5','1987-12-28','alek.lind@hotmail.com',NULL,'$2y$10$4QxD5XzHPJfbQCdyBafVhuOuvPNK7hRxGtY6HPA/3s/aSJSWU9JQC',NULL,'2020-05-15 23:05:30','2020-05-15 23:05:30'),(21,'Coreas Hernandez','22552255','hernandez87','1999-12-31','jmardoqueo94@gmail.com',NULL,'$2y$10$g88EjPbxg/31/fi7apaBlOqddAwDe4rJBb4jCjZPWzZF29igqpvde',NULL,'2020-05-16 02:25:18','2020-05-16 03:39:23'),(22,'Usuario Test','22552255','test2020','1980-12-22','test@gmail.com',NULL,'$2y$10$FRkGB9pUY6Wjz5E4pyWJEOVDrI/K3HCVUDXIIGJnzcoe9C1ytyJbC',NULL,'2020-05-17 02:05:26','2020-05-17 02:05:26');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-16 15:06:02
