<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class create_products extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i=0; $i < 20; $i++) {
            \DB::table('products')->insert(array(
                'name' => $faker->word,
                'sku'  => $faker->unique()->randomNumber($nbDigits = 5),
                'quantity' => $faker->randomDigit($nbDigits = 1),
                'price' => $faker->randomFloat($nbMaxDecimals = 2, $min = 1.00, $max = 50.00),
                'description' => 'Product Description',
                'image' => 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTxybTeJCkLvNnZEZgG0IKtkj6GVj5gfu_A4i99IOvF4BDDI4fA&usqp=CAU',
                'created_at' => date('Y-m-d H:m:s'),
                'updated_at' => date('Y-m-d H:m:s')
            ));
        }
    }
}
