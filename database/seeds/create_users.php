<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class create_users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker::create();

        \DB::table('users')->insert(array(
            'name' => 'Usuario Test',
            'phone'  => '22552255',
            'username' => 'test2020',
            'fecha_nacimiento' => $faker->date($format = 'Y-m-d', $max = '2000-12-31'),
            'email' => 'test@gmail.com',
            'password' => bcrypt('123456'),
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ));

        for ($i=0; $i < 20; $i++) {
            \DB::table('users')->insert(array(
                'name' => $faker->firstNameMale.' '.$faker->lastName,
                'phone'  => $faker->phoneNumber,
                'username' => $faker->firstNameMale.''.$faker->randomDigit($nbDigits = 1),
                'fecha_nacimiento' => $faker->date($format = 'Y-m-d', $max = '2000-12-31'),
                'email' => $faker->email,
                'password' => bcrypt('123456'),
                'created_at' => date('Y-m-d H:m:s'),
                'updated_at' => date('Y-m-d H:m:s')
            ));
        }
    }
}
