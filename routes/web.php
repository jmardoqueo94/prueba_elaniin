<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//Route::get('list_users', 'UserController@getUsers');

// Route::get('/home', 'HomeController@index')->name('home');

//Rutas para el login

/*Route::post('login_user', 'Auth\LoginController@login')->name('login_user');

Route::post('register_user', 'Auth\RegisterController@register')->name('register_user');
 
Route::group(['middleware' => 'jwt', "prefix" => "api"], function () {
    Route::get('logout', 'UserController@logout');

    //Rutas para usuarios
    Route::get('list_users', 'UserController@getUsers')->name('list_users');
    Route::get('get_user/{id}', 'UserController@show_user')->name('get_user');
    Route::post('edit_user', 'UserController@editUser')->name('edit_user');
    Route::delete('remove_user/{id}', 'UserController@removeUser')->name('remove_user');

    //Rutas para productos
    Route::get('list_products', 'ProductController@getProducts')->name('list_products');
    Route::get('get_product/{id}', 'ProductController@show_product')->name('get_product');
    Route::post('product_save', 'ProductController@createProduct')->name('product_save');
    Route::post('product_edit', 'ProductController@editProduct')->name('product_edit');
    Route::delete('product_remove/{id}', 'ProductController@remove')->name('product_remove');
});*/