Este es el proyecto con el API que solicitaban en la prueba para backend Laravel.

Está hecho en Laravel 6 y MySQL 5.7.

Para la autenticación usé JWT.

En el proyecto va incluído el dump de la base de datos con el nombre "prueba_elaniin.sql".

Van las migraciones para las tablas y 2 archivos seeder para llenar las tablas user y products.

Va una colección de endpoints probados localmente en POSTMAN con el nombre "Prueba_elaniin.postman_collection.json".

Para probar los endpoint de manera correcta, primero se debe ejecutar el de login_user para obtener el token necesario que se envía en cada petición.

Y otra colección colección de endpoints probados en el server en POSTMAN con el nombre "Prueba_elaniin_server.postman_collection.json".

Las validaciones están hechas tal y como lo decía en el documento de la prueba para ingresar un usuario y producto, con los campos requeridos y tipo de dato.

El server donde está alojado el API es: http://prueba-elaniin.photorevstudio.com, en la colección de endpoints del server van las rutas de cada uno.

La URL del repositorio es: https://gitlab.com/jmardoqueo94/prueba_elaniin.git