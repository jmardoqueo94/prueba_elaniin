@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-6">
        </div>
        <div class="col-md-6">
            <div class="input-group mb-2">
                <input type="text" class="form-control" placeholder="Search Data" aria-label="Recipient's username" aria-describedby="basic-addon2">
                <div class="input-group-append">
                    <button class="btn btn-outline-secondary" type="button">Button</button>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="form-group row">
        <label for="inputPassword" class="col-sm-1 col-form-label">Buscar</label>
        <div class="col-md-10">
        <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for names..">
        </div>
    </div> -->
    <table class="table table-bordered">
        <thead>
            <tr>
            <th scope="col">ID</th>
            <th scope="col">SKU</th>
            <th scope="col">Name</th>
            <th scope="col">Quantity</th>
            <th scope="col">Price</th>
            <th scope="col">Description</th>
            <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($product as $prod)
                <tr>
                    <th scope="row">{{ $prod->id }}</th>
                    <td>{{ $prod['sku'] }}</td>
                    <td>{{ $prod['name'] }}</td>
                    <td>{{ $prod['quantity'] }}</td>
                    <td>{{ $prod['price'] }}</td>
                    <td>{{ $prod['description'] }}</td>
                    <td>    
                        <button type="button" class="btn btn-success"><i class="fa fa-edit"></i></button>
                        <button type="button" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $product->links() }}
@stop