@extends('layouts.app')

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <table id="users" class="table table-bordered data-table">
        <thead>
            <tr>
            <th scope="col">#</th>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">Phone</th>
            <th scope="col">Date Birthday</th>
            <th scope="col">Username</th>
            <th scope="col">Email</th>
            <th width="100px">Action</th>
            </tr>
        </thead>
        <tbody>
            
        </tbody>
    </table>

    <script type="text/javascript">
    
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('#users').DataTable({
                processing: true,
                ServerSide: true,
                ajax:{
                    "url" : "{{ route('list_users') }}",
                    "type" : "GET",
                    /*"dataSrc": function (d) {
                        return d
                    },*/
                    "headers": {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    "data" : {
                        '_token' : "{{ csrf_token() }}",
                    },
                },
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'phone', name: 'phone'},
                    {data: 'fecha_nacimiento', name: 'fecha_nacimiento'},
                    {data: 'username', name: 'username'},
                    {data: 'email', name: 'email'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            }); 
        });

    </script>
@stop